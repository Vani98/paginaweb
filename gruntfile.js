module.exports= function(grunt){
    // con grunt solo esto se saca
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt,{
        useminPrepare:'grunt-usemin'
    // termina
    });
    // configurar la libreria de sas 
    grunt.initConfig({
        sass:{
            // configura la herramienta de sas para que genere la version de distribucion buscando en todos los archivos de la carpeta css con la extension scss y los mande desntro del destino que es la carpeta css y con la extension css
            dist:{
                files:[{
                    expand:true,
                    cwd:'css',
                    src:['*.scss'],
                    dest:'css',
                    ext:'.css'
                }]
            }
        },
        watch:{
            files:['css/*.scss'],
            tasks:['css']
        },
        browserSync:{
            dev:{
                bsFiles:{ //browser files
                    // indicamos archivos que va a mirar
                    src:[
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options:{
                    watchTask: true,
                    server:{
                        baseDir: './' //Directorio base para nuestro servidor
                    }
                }
            }
        },
        imagemin:{
            dynamic:{
                files:[{
                    expand: true,
                    cwd: './',
                    src:'img/*.{png,gif,jpg,jpeg}',
                    dest:'dist/'
                }]
            }
        },
        //con grunt solo esto se saca
        copy: {
            html:{
                files:[{
                    expand:true,
                    dot:true,
                    cwd:'./',  //current working directory
                    src:['*.html'],
                    dest:'dist'
                }]
            },
        },
        clean:{
            build:{
                src:['dist/'] //clean the distribution folder
            }
        },
        cssmin:{
            dist:{}
        },
        uglify:{
            dist:{}
        },
        filerev:{
            options:{
                encoding:'utf8',
                algorithm:'md5',
                length:20
            },
            release:{
                // filerev:release hashes(md6) all assets(images, js and css)
            //in dist directory
                files:[{
                    src:[
                        'dist/js/*.js',
                        'dist/css/*.css',
                    ]
                }]
            }
        },
        concat:{
            options:{
                separator:';'
            },
            dist:{}
        },
        useminPrepare:{
            foo:{
                dest:'dist',
                src:['index.html','contacto.html','envio.html','nosotros.html','terminos-y-condiciones.html']
            },
            options:{
                flow:{
                    steps:{
                        css:['cssmin'],
                        js:['uglify'],
                    },
                    post:{
                        css:[{
                            name:'cssmin',
                            createConfig: function(context,block){
                                var generated= context.options.generated;
                                generated.options={
                                    keepSpecialComments:0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },
        usemin:{
            html:['dist/index.html','dist/contacto.html','dist/envio.html','dist/nosotros.html','dist/terminos-y-condiciones.html'],
            options:{
                assetsDir:['dist','dist/css','dist/js']
            }
        }
        //termina
    });
    // siempre cuando usamos grunt se agregan las tareas, paquetes o pluggins


    // grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-contrib-sass');
    // grunt.loadNpmTasks('grunt-browser-sync');
    // grunt.loadNpmTasks('grunt-contrib-imagemin');

    grunt.registerTask('css',['sass']);
    grunt.registerTask('default',['browserSync','watch']);
    grunt.registerTask('img:compress',['imagemin']);

//con grunt solo esto se saca
    grunt.registerTask('build',[
        'clean', //Borramos el contenido de dist
        'copy', //Copiamos los archivos html a dist
        'imagemin',  //Optimizamos imagenes y las copiamos a dist
        'useminPrepare', //Preparamos la configuracion de usemin
        'concat',
        'cssmin',
        'uglify',
        'filerev', //Agregamos cadena aleatoria
        'usemin'  //Reemplazamos las referencias por los archivos generados por filerev
    ])
//termina aca
};