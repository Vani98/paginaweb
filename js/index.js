$(function () {
    $('.carousel').carousel({
        interval: 2800
    });

    $("#contacto").on("show.bs.modal", function (e) {
        console.log("el modal se está mostrando");
        $("#contactobtn").removeClass("btn-primary");
        $("#contactobtn").addClass("btn-success");
        $("#contactobtn").prop("disabled", true);
    });
    $("#contacto").on("hide.bs.modal", function (e) {
        console.log("el modal se esta ocultando");
    });
    $("#contacto").on("hidden.bs.modal", function (e) {
        console.log("el modal se ocultó");
        $("#contactobtn").prop("disabled", false);
    });
});

$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
});